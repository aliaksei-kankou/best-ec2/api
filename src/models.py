from flask_restx import fields


from custom_types import (
    UsageClass,
    GpuAvailability,
    BurstablePerformanceAvailability,
    ProductDescription,
    CpuArchitecture,
    HardwareGeneration,
    InstanceStorageAvailability,
    SpotPriceStrategy,
)
from utils import enum_field


class CustomNested(fields.Nested):
    def output(self, key, obj, ordered=False, **kwargs):
        data = super().output(key, obj, ordered=ordered, **kwargs)
        # If the data is a dict and all its values are None, return None instead
        if isinstance(data, dict) and all(value is None for value in data.values()):
            return None
        return data


def get_models(api):
    instance_storage_model = api.model(
        "InstanceStorage",
        {
            "sizeInGb": fields.Integer(
                required=True, description="The storage size in gigabytes"
            ),
            "count": fields.Integer(
                required=True, description="The number of storage devices"
            ),
            "type": fields.String(
                required=True, description="The storage type, e.g., SSD"
            ),
        },
    )

    interruption_frequency_model = api.model(
        "InterruptionFrequency",
        {
            "min": fields.Integer(
                required=True, description="The minimum frequency of interruptions"
            ),
            "max": fields.Integer(
                required=True, description="The maximum frequency of interruptions"
            ),
            "rate": fields.String(
                required=True,
                description="The rate of interruptions expressed as a string",
            ),
        },
    )

    instance_model = api.model(
        "Instance",
        {
            "instanceType": fields.String(
                required=True,
                description="The type of the instance, indicating the combination of CPU, memory, and other "
                "resources allocated. Example types include general purpose, compute optimized, "
                "and memory optimized instances.",
                example="g4ad.xlarge",
            ),
            "virtualCpus": fields.Integer(
                required=True,
                description="The number of virtual CPUs (vCPUs) available to the instance, determining its "
                "computational power.",
                example=4,
            ),
            "memoryInGiB": fields.Integer(
                required=True,
                description="The amount of memory allocated to the instance, measured in gibibytes (GiB).",
                example=16,
            ),
            "gpuMemoryInGiB": fields.Integer(
                required=False,
                description="The amount of GPU memory, if applicable, allocated to the instance, measured "
                "in gibibytes (GiB). This field is optional and relevant only for instances with "
                "GPU capabilities.",
                example=1,
            ),
            "gpus": fields.Integer(
                required=False,
                description="The number of graphical processing units (GPUs) available in the instance type, "
                "which can enhance graphics and computational tasks.",
                example=1,
            ),
            "networkPerformance": fields.String(
                required=True,
                description="Describes the network performance capability of the instance.",
                example="Up to 25 Gigabit",
            ),
            "price": fields.Float(
                required=True,
                description="The hourly price of the instance in USD.",
                example=0.47327,
            ),
            "azPrice": fields.Raw(
                required=False,
                skip_none=True,
                description="A dictionary mapping each Availability Zone (AZ) to its specific price for the "
                "instance, allowing users to compare costs across different zones.",
                example={"eu-central-1a": 0.0426, "eu-central-1b": 0.0496},
            ),
            "interruptionFrequency": CustomNested(
                interruption_frequency_model,
                skip_null=True,
                required=False,
                description="An optional object detailing the frequency of interruptions. This is "
                "particularly relevant for spot or preemptible instances, providing a min "
                "and max frequency and an average rate.",
                example={"min": 0, "max": 5, "rate": "<5%"},
            ),
            "instanceStorage": fields.List(
                fields.Nested(instance_storage_model),
                required=False,
                description="A list detailing each storage device attached to the instance. This field is "
                "optional and varies based on instance type.",
                example=[{"sizeInGb": 1200, "count": 2, "type": "ssd"}],
            ),
        },
    )

    instance_request_model = api.model(
        "InstanceRequest",
        {
            "virtualCpus": fields.Float(
                required=True,
                description="The number of virtual CPUs allocated to the instance.",
                example=1,
            ),
            "memoryInGiB": fields.Float(
                required=True,
                description="The amount of RAM in gibibytes allocated to the instance.",
                example=1,
            ),
            "usageClass": enum_field(
                "Specifies whether the instance is spot or on-demand.",
                [member.value for member in UsageClass.__members__.values()],
                UsageClass.ON_DEMAND,
                UsageClass.ON_DEMAND,
            ),
            "gpuAvailability": enum_field(
                "Indicates the availability status of GPU.",
                [member.value for member in GpuAvailability.__members__.values()],
                GpuAvailability.AVAILABLE,
            ),
            "gpuMemoryInGiB": fields.Integer(
                required=False,
                description="Specifies the GPU memory size in gibibytes. Relevant only when "
                "'gpuAvailability' is 'available'.",
                example=1,
            ),
            "gpus": fields.Integer(
                required=False,
                description="Specifies total GPUs. Relevant only when 'gpuAvailability' is 'available'.",
                example=1,
            ),
            "burstablePerformanceAvailability": enum_field(
                "Indicates if burst capability is available.",
                [
                    member.value
                    for member in BurstablePerformanceAvailability.__members__.values()
                ],
                BurstablePerformanceAvailability.ANY,
            ),
            "cpuArchitecture": enum_field(
                "The CPU architecture of the instance.",
                [member.value for member in CpuArchitecture.__members__.values()],
                CpuArchitecture.X86_64,
                CpuArchitecture.X86_64,
            ),
            "osProductDescription": enum_field(
                "The operating system and its specific version.",
                [member.value for member in ProductDescription.__members__.values()],
                ProductDescription.LINUX_UNIX,
                ProductDescription.LINUX_UNIX,
            ),
            "hardwareGeneration": enum_field(
                "Specifies the hardware generation of the instance.",
                [member.value for member in HardwareGeneration.__members__.values()],
                HardwareGeneration.CURRENT,
            ),
            "instanceStorageAvailability": enum_field(
                "Indicates the availability of instance storage.",
                [
                    member.value
                    for member in InstanceStorageAvailability.__members__.values()
                ],
                InstanceStorageAvailability.ANY,
            ),
            "spotPriceStrategy": enum_field(
                "Strategy for spot price bidding. Relevant only when 'usageClass' is 'spot'.",
                [member.value for member in SpotPriceStrategy.__members__.values()],
                SpotPriceStrategy.MIN,
                SpotPriceStrategy.MIN,
            ),
            "maxInterruptionFrequencyPercent": fields.Integer(
                required=False,
                description="Defines the maximum frequency of interruptions for spot instances. "
                "Relevant only when 'usageClass' is 'spot'.",
                min=0,
                max=100,
                example=10,
            ),
            "availabilityZones": fields.List(
                fields.String,
                required=False,
                description="Preferred availability zones for instance deployment. Relevant only when "
                "'usageClass' is 'spot'.",
                example=["us-east-1a", "us-east-1b", "us-east-1c"],
                default="All availability zones located in the chosen region",
            ),
            "region": fields.String(
                required=False,
                description="Specifies the region where the instance should be deployed.",
                example="us-east-1",
                default="us-east-1",
            ),
        },
    )
    instance_response_model = api.model(
        "InstanceResponse", {"instances": fields.List(fields.Nested(instance_model))}
    )

    regions_response_model = api.model(
        "RegionsResponse",
        {
            "regions": fields.List(
                fields.String(
                    enum=[
                        "ap-south-1",
                        "eu-north-1",
                        "eu-west-3",
                        "eu-west-2",
                        "eu-west-1",
                        "ap-northeast-3",
                        "ap-northeast-2",
                        "ap-northeast-1",
                        "ca-central-1",
                        "sa-east-1",
                        "ap-southeast-1",
                        "ap-southeast-2",
                        "eu-central-1",
                        "us-east-1",
                        "us-east-2",
                        "us-west-1",
                        "us-west-2",
                    ],
                    description="AWS region identifier",
                ),
                required=True,
                description="List of AWS region identifiers",
            )
        },
    )

    availability_zones_response_model = api.model(
        "AvailabilityZonesResponse",
        {
            "availabilityZones": fields.List(
                fields.String,
                description="List of availability zones for the specified region",
                example=[
                    "us-east-1a",
                    "us-east-1b",
                    "us-east-1c",
                    "us-east-1d",
                    "us-east-1e",
                    "us-east-1f",
                ],
            )
        },
    )

    return (
        instance_request_model,
        instance_response_model,
        regions_response_model,
        availability_zones_response_model,
    )
