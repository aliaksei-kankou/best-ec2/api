import logging

from flask import request, abort
from pydantic import ValidationError
from flask_restx import Resource

from converters import dto_to_instance_request, instance_response_to_dto
from services import AwsService
from custom_types import (
    InstanceRequestDto,
    InstanceResponseDto,
    AwsRegion,
    RegionsDto,
    AvailabilityZonesDto,
)
from models import get_models


# pylint: disable=unused-variable
def initialize_routes(api):
    (
        instance_request_model,
        instance_response_model,
        regions_response_model,
        availability_zones_response_model,
    ) = get_models(api)

    @api.route("/instances")
    class Instances(Resource):
        @api.expect(instance_request_model)
        @api.marshal_with(instance_response_model)
        def post(self) -> InstanceResponseDto.dict:
            try:
                instance_request_dto = InstanceRequestDto(**request.json)
            except ValidationError as e:
                abort(400, description=str(e))

            instance_request = dto_to_instance_request(instance_request_dto)

            logging.info(instance_request)

            response = AwsService.get_best_instance(instance_request)
            return instance_response_to_dto(response).dict()

    @api.route("/regions")
    class Regions(Resource):
        @api.doc(
            description="List all AWS regions",
            responses={200: ("Success", "RegionsResponse")},
        )
        @api.marshal_with(regions_response_model)
        def get(self) -> RegionsDto.dict:
            """List all AWS regions"""
            return RegionsDto(regions=AwsService.get_aws_regions()).dict()

    @api.route("/availability-zones/<string:region>")
    class AvailabilityZones(Resource):
        @api.doc(
            params={
                "region": {
                    "description": "The AWS region identifier",
                    "enum": [region.value for region in AwsRegion.__members__.values()],
                    "in": "path",
                }
            },
            description="List availability zones for a specific region",
            responses={200: ("Success", "AvailabilityZonesResponse")},
        )
        @api.marshal_with(availability_zones_response_model)
        def get(self, region: str):
            """List availability zones for a specific region"""
            try:
                region_enum = AwsRegion(region)
            except ValueError:
                valid_values = ", ".join(
                    [region.value for region in AwsRegion.__members__.values()]
                )
                api.abort(
                    400,
                    f"Invalid AWS region identifier: {region}. Valid values are: {valid_values}",
                )

            availability_zones = AwsService.get_availability_zones(region_enum)
            return AvailabilityZonesDto(availabilityZones=availability_zones).dict()
