from best_ec2 import (
    InstanceTypeRequest,
    InstanceTypeResponse,
    UsageClass,
    Architecture,
    FinalSpotPriceStrategy,
    ProductDescription,
)

from custom_types import (
    InstanceRequestDto,
    BurstablePerformanceAvailability,
    GpuAvailability,
    HardwareGeneration,
    InstanceStorageAvailability,
    InstanceResponseDto,
    InstanceEntry,
    InstanceStorage,
)


def dto_to_instance_request(dto: InstanceRequestDto) -> InstanceTypeRequest:
    return InstanceTypeRequest(
        vcpu=dto.virtualCpus,
        memory_gb=dto.memoryInGiB,
        usage_class=UsageClass(dto.usageClass).value if dto.usageClass else None,
        burstable=None
        if dto.burstablePerformanceAvailability == BurstablePerformanceAvailability.ANY
        else dto.burstablePerformanceAvailability
        == BurstablePerformanceAvailability.AVAILABLE,
        architecture=Architecture(dto.cpuArchitecture).value
        if dto.cpuArchitecture
        else None,
        product_description=ProductDescription(dto.osProductDescription).value
        if dto.osProductDescription
        else None,
        is_current_generation=None
        if dto.hardwareGeneration == HardwareGeneration.ANY
        else dto.hardwareGeneration == HardwareGeneration.CURRENT,
        has_gpu=None
        if dto.gpuAvailability == GpuAvailability.ANY
        else dto.gpuAvailability == GpuAvailability.AVAILABLE,
        gpu_memory=dto.gpuMemoryInGiB,
        is_instance_storage_supported=None
        if dto.instanceStorageAvailability == InstanceStorageAvailability.ANY
        else dto.instanceStorageAvailability == InstanceStorageAvailability.SUPPORTED,
        max_interruption_frequency=dto.maxInterruptionFrequencyPercent,
        availability_zones=dto.availabilityZones,
        final_spot_price_strategy=FinalSpotPriceStrategy(dto.spotPriceStrategy).value
        if dto.spotPriceStrategy
        else None,
        region=dto.region,
    )


def instance_response_to_dto(response: InstanceTypeResponse) -> InstanceResponseDto:
    response_dto = [
        InstanceEntry(
            instanceType=entry.get("instance_type"),
            virtualCpus=entry.get("vcpu"),
            memoryInGiB=entry.get("memory_gb"),
            networkPerformance=entry.get("network_performance"),
            instanceStorage=(
                list(
                    map(
                        lambda storage: InstanceStorage(
                            sizeInGb=storage.get("SizeInGB"),
                            count=storage.get("Count"),
                            type=storage.get("Type"),
                        ),
                        entry.get("storage", []),
                    )
                )
                if isinstance(entry.get("storage"), list)
                else None
            ),
            price=entry.get("price"),
            azPrice=entry.get("az_price"),
            interruptionFrequency=entry.get("interruption_frequency"),
            gpuMemoryInGiB=entry.get("gpu_memory_gb"),
            gpus=entry.get("gpus"),
        )
        for entry in response
    ]
    return InstanceResponseDto(instances=response_dto)
