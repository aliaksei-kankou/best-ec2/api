from typing import Optional, Dict
from enum import Enum

from pydantic import BaseModel, Field


class GpuAvailability(str, Enum):
    AVAILABLE = "available"
    UNAVAILABLE = "unavailable"
    ANY = "any"


class BurstablePerformanceAvailability(str, Enum):
    AVAILABLE = "available"
    UNAVAILABLE = "unavailable"
    ANY = "any"


class HardwareGeneration(str, Enum):
    CURRENT = "current"
    PREVIOUS = "previous"
    ANY = "any"


class UsageClass(str, Enum):
    SPOT = "spot"
    ON_DEMAND = "on-demand"


class CpuArchitecture(str, Enum):
    I386 = "i386"
    X86_64 = "x86_64"
    ARM64 = "arm64"
    X86_64_MAC = "x86_64_mac"


class SpotPriceStrategy(str, Enum):
    MIN = "min"
    MAX = "max"
    AVERAGE = "average"


class ProductDescription(str, Enum):
    LINUX_UNIX = "Linux/UNIX"
    RED_HAT_LINUX = "Red Hat Enterprise Linux"
    SUSE_LINUX = "SUSE Linux"
    WINDOWS = "Windows"
    LINUX_UNIX_VPC = "Linux/UNIX (Amazon VPC)"
    RED_HAT_LINUX_VPC = "Red Hat Enterprise Linux (Amazon VPC)"
    SUSE_LINUX_VPC = "SUSE Linux (Amazon VPC)"
    WINDOWS_VPC = "Windows (Amazon VPC)"


class InstanceStorageAvailability(str, Enum):
    SUPPORTED = "available"
    NOT_SUPPORTED = "unavailable"
    ANY = "any"


class InstanceRequestDto(BaseModel):
    virtualCpus: float = Field(ge=0)
    memoryInGiB: float = Field(ge=0)
    usageClass: Optional[UsageClass] = None
    gpuAvailability: Optional[GpuAvailability] = None
    gpuMemoryInGiB: Optional[int] = Field(None, ge=0)
    gpus: Optional[int] = Field(None, ge=0)
    burstablePerformanceAvailability: Optional[BurstablePerformanceAvailability] = None
    cpuArchitecture: Optional[CpuArchitecture] = None
    osProductDescription: Optional[ProductDescription] = None
    hardwareGeneration: Optional[HardwareGeneration] = None
    instanceStorageAvailability: Optional[InstanceStorageAvailability] = None
    spotPriceStrategy: Optional[SpotPriceStrategy] = None
    maxInterruptionFrequencyPercent: Optional[int] = Field(None, ge=0, le=100)
    availabilityZones: Optional[list[str]] = None
    region: Optional[str] = None


class InterruptionFrequency(BaseModel):
    min: int
    max: int
    rate: str


class InstanceStorage(BaseModel):
    sizeInGb: int
    count: int
    type: str


class InstanceEntry(BaseModel):
    instanceType: str
    virtualCpus: int
    memoryInGiB: int
    networkPerformance: str
    instanceStorage: Optional[list[InstanceStorage]] = None
    price: float
    azPrice: Optional[Dict[str, float]] = None
    interruptionFrequency: Optional[InterruptionFrequency] = None
    gpuMemoryInGiB: Optional[int] = None
    gpus: Optional[int] = None


class InstanceResponseDto(BaseModel):
    instances: list[InstanceEntry]


class AwsRegion(str, Enum):
    AP_SOUTH_1 = "ap-south-1"
    EU_NORTH_1 = "eu-north-1"
    EU_WEST_3 = "eu-west-3"
    EU_WEST_2 = "eu-west-2"
    EU_WEST_1 = "eu-west-1"
    AP_NORTHEAST_3 = "ap-northeast-3"
    AP_NORTHEAST_2 = "ap-northeast-2"
    AP_NORTHEAST_1 = "ap-northeast-1"
    CA_CENTRAL_1 = "ca-central-1"
    SA_EAST_1 = "sa-east-1"
    AP_SOUTHEAST_1 = "ap-southeast-1"
    AP_SOUTHEAST_2 = "ap-southeast-2"
    EU_CENTRAL_1 = "eu-central-1"
    US_EAST_1 = "us-east-1"
    US_EAST_2 = "us-east-2"
    US_WEST_1 = "us-west-1"
    US_WEST_2 = "us-west-2"


class RegionsDto(BaseModel):
    regions: list[str]


class AvailabilityZonesDto(BaseModel):
    availabilityZones: list[str]
