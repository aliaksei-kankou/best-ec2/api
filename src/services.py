from typing import List
import logging

import boto3
from best_ec2 import BestEc2, InstanceTypeRequest, InstanceTypeResponse

from custom_types import AwsRegion

# Initialize best_ec2 here, so it can be shared across the application
best_ec2 = BestEc2()


class AwsService:
    @staticmethod
    def get_best_instance(request: InstanceTypeRequest) -> InstanceTypeResponse:
        logging.info(request)
        return best_ec2.get_types(request)

    @staticmethod
    def get_aws_regions() -> List[str]:
        ec2_client = boto3.client("ec2", region_name="us-east-1")
        response = ec2_client.describe_regions()
        return [region["RegionName"] for region in response["Regions"]]

    @staticmethod
    def get_availability_zones(region: AwsRegion) -> List[str]:
        ec2_client = boto3.client("ec2", region_name=region)
        response = ec2_client.describe_availability_zones()
        return [zone["ZoneName"] for zone in response["AvailabilityZones"]]
