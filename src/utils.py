from flask_restx import fields


def enum_field(
    description,
    enum_values,
    example=None,
    default=None,
):
    return fields.String(
        required=False,
        description=description,
        enum=enum_values,
        example=example,
        default=default,
    )
