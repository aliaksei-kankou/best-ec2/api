import logging

from flask import Flask
from flask_restx import Api

from resources import initialize_routes

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)


def create_app():
    app = Flask(__name__)
    api = Api(
        app,
        title="SunSpace.Pro Best EC2 API (best-ec2.com)",
        version="0.0.1",
        description="",
    )

    initialize_routes(api)

    return app
