import pytest
from unittest.mock import patch

from best_ec2 import InstanceTypeRequest, InstanceType, DiskInfo

from services import AwsService, best_ec2

mock_instance_types = {
    (2, 4): [
        InstanceType(
            instance_type="t2.micro",
            price=0.0116,
            vcpu=2,
            memory_gb=4,
            network_performance="Up to 10 Gigabit",
            storage="EBS Only",
        )
    ],
    (4, 8): [
        InstanceType(
            instance_type="t3a.xlarge",
            price=0.172,
            vcpu=4,
            memory_gb=8,
            network_performance="Up to 10 Gigabit",
            storage=[DiskInfo(SizeInGB=150, Count=1, Type="ssd")],
        )
    ],
}


@pytest.fixture
def mock_ec2_client():
    with patch("boto3.client", autospec=True) as mock_boto3_client:
        yield mock_boto3_client.return_value


@pytest.mark.parametrize(
    "vcpu, memory_gb, expected_response",
    [
        (2, 4, mock_instance_types[(2, 4)]),
        (4, 8, mock_instance_types[(4, 8)]),
    ],
    ids=["cpu2_memory4", "cpu4_memory8"],
)
def test_get_best_instance(vcpu, memory_gb, expected_response, mock_ec2_client):
    instance_request = InstanceTypeRequest(vcpu=vcpu, memory_gb=memory_gb)
    with patch.object(
        best_ec2, "get_types", return_value=expected_response
    ) as mock_get_types:
        # Act
        response = AwsService.get_best_instance(instance_request)

        # Assert
        mock_get_types.assert_called_once_with(instance_request)
        assert response == expected_response


@pytest.mark.parametrize(
    "expected_regions, mock_response",
    [
        (
            ["us-east-1", "eu-west-1"],
            {"Regions": [{"RegionName": "us-east-1"}, {"RegionName": "eu-west-1"}]},
        ),
        (
            ["us-east-2", "eu-central-1"],
            {"Regions": [{"RegionName": "us-east-2"}, {"RegionName": "eu-central-1"}]},
        ),
    ],
    ids=["regions_set_1", "regions_set_2"],
)
def test_get_aws_regions(expected_regions, mock_response, mock_ec2_client):
    mock_ec2_client.describe_regions.return_value = mock_response

    # Act
    regions = AwsService.get_aws_regions()

    # Assert
    mock_ec2_client.describe_regions.assert_called_once()
    assert regions == expected_regions


@pytest.mark.parametrize(
    "region, expected_zones, mock_response",
    [
        (
            "us-east-1",
            ["us-east-1a", "us-east-1b", "us-east-1c"],
            {
                "AvailabilityZones": [
                    {"ZoneName": "us-east-1a"},
                    {"ZoneName": "us-east-1b"},
                    {"ZoneName": "us-east-1c"},
                ]
            },
        ),
        (
            "eu-central-1",
            ["eu-central-1a", "eu-central-1b", "eu-central-1c"],
            {
                "AvailabilityZones": [
                    {"ZoneName": "eu-central-1a"},
                    {"ZoneName": "eu-central-1b"},
                    {"ZoneName": "eu-central-1c"},
                ]
            },
        ),
    ],
    ids=["us-east-1_zones", "eu-central-1_zones"],
)
def test_get_availability_zones(region, expected_zones, mock_response, mock_ec2_client):
    mock_ec2_client.describe_availability_zones.return_value = mock_response

    # Act
    zones = AwsService.get_availability_zones(region)

    # Assert
    mock_ec2_client.describe_availability_zones.assert_called_once()
    assert zones == expected_zones
