import pydantic_core
import pytest

from best_ec2 import (
    InstanceTypeRequest,
    Architecture,
    FinalSpotPriceStrategy,
)

from custom_types import (
    InstanceRequestDto,
    BurstablePerformanceAvailability,
    GpuAvailability,
    HardwareGeneration,
    InstanceStorageAvailability,
    UsageClass,
    CpuArchitecture,
    ProductDescription,
    SpotPriceStrategy,
    InstanceResponseDto,
    InstanceEntry,
    InstanceStorage,
    InterruptionFrequency,
)
from converters import dto_to_instance_request, instance_response_to_dto


@pytest.mark.parametrize(
    "dto, expected",
    [
        # Happy path tests
        pytest.param(
            InstanceRequestDto(
                virtualCpus=4,
                memoryInGiB=16,
                usageClass=UsageClass.ON_DEMAND,
                burstablePerformanceAvailability=BurstablePerformanceAvailability.AVAILABLE,
                cpuArchitecture=CpuArchitecture.X86_64,
                osProductDescription=ProductDescription.LINUX_UNIX,
                hardwareGeneration=HardwareGeneration.CURRENT,
                gpuAvailability=GpuAvailability.AVAILABLE,
                gpuMemoryInGiB=8,
                instanceStorageAvailability=InstanceStorageAvailability.SUPPORTED,
                maxInterruptionFrequencyPercent=10,
                availabilityZones=["us-east-1a", "us-east-1b"],
                spotPriceStrategy=SpotPriceStrategy.MIN,
                region="us-east-1",
            ),
            InstanceTypeRequest(
                vcpu=4,
                memory_gb=16,
                usage_class=UsageClass.ON_DEMAND.value,
                burstable=True,
                architecture=Architecture.X86_64.value,
                product_description=ProductDescription.LINUX_UNIX.value,
                is_current_generation=True,
                has_gpu=True,
                gpu_memory=8,
                is_instance_storage_supported=True,
                max_interruption_frequency=10,
                availability_zones=["us-east-1a", "us-east-1b"],
                final_spot_price_strategy=FinalSpotPriceStrategy.MIN.value,
                region="us-east-1",
            ),
            id="happy_path_all_fields_provided",
        ),
        # Edge cases
        pytest.param(
            InstanceRequestDto(
                virtualCpus=1,
                memoryInGiB=0.5,
                usageClass=None,
                burstablePerformanceAvailability=BurstablePerformanceAvailability.ANY,
                cpuArchitecture=None,
                osProductDescription=None,
                hardwareGeneration=HardwareGeneration.ANY,
                gpuAvailability=GpuAvailability.ANY,
                gpuMemoryInGiB=0,
                instanceStorageAvailability=InstanceStorageAvailability.ANY,
                maxInterruptionFrequencyPercent=0,
                availabilityZones=[],
                spotPriceStrategy=None,
                region="",
            ),
            InstanceTypeRequest(
                vcpu=1,
                memory_gb=0.5,
                usage_class=None,
                burstable=None,
                architecture=None,
                product_description=None,
                is_current_generation=None,
                has_gpu=None,
                gpu_memory=0,
                is_instance_storage_supported=None,
                max_interruption_frequency=0,
                availability_zones=[],
                final_spot_price_strategy=None,
                region="",
            ),
            id="edge_case_minimum_values",
        ),
    ],
)
def test_dto_to_instance_request(dto, expected):
    # Act
    result = dto_to_instance_request(dto)

    # Assert
    assert result == expected, f"Expected {expected} but got {result}"


@pytest.mark.parametrize(
    "response, expected",
    [
        # Happy path tests
        pytest.param(
            [
                {
                    "instance_type": "t2.micro",
                    "vcpu": 1,
                    "memory_gb": 1,
                    "network_performance": "Low",
                    "storage": [{"SizeInGB": 8, "Count": 1, "Type": "SSD"}],
                    "price": 0.0116,
                    "az_price": {"us-east-1": 0.012, "us-east-2": 0.015},
                    "interruption_frequency": {"min": 0, "max": 5, "rate": "<5%"},
                    "gpu_memory_gb": 0,
                    "gpus": 0,
                }
            ],
            InstanceResponseDto(
                instances=[
                    InstanceEntry(
                        instanceType="t2.micro",
                        virtualCpus=1,
                        memoryInGiB=1,
                        networkPerformance="Low",
                        instanceStorage=[
                            InstanceStorage(sizeInGb=8, count=1, type="SSD")
                        ],
                        price=0.0116,
                        azPrice={"us-east-1": 0.012, "us-east-2": 0.015},
                        interruptionFrequency=InterruptionFrequency(
                            min=0, max=5, rate="<5%"
                        ),
                        gpuMemoryInGiB=0,
                        gpus=0,
                    )
                ]
            ),
            id="happy_path_single_instance",
        ),
        # Edge cases
        pytest.param(
            [],
            InstanceResponseDto(instances=[]),
            id="edge_case_empty_response",
        ),
    ],
)
def test_instance_response_to_dto(response, expected):
    # Act
    result = instance_response_to_dto(response)

    # Assert
    assert result == expected, f"Expected {expected} but got {result}"


def test_instance_response_to_dto_error_case():
    # Arrange
    invalid_response = [
        {
            "instance_type": "t2.micro",
            "vcpu": 1,
            "memory_gb": 1,
            "network_performance": "Low",
            "storage": [{"SizeInGB": 8, "Count": 1, "Type": "SSD"}],
            "price": 0.0116,
            "az_price": {"us-east-1": 0.012, "us-east-2": 0.015},
            "interruption_frequency": "ErrorValue",
            "gpu_memory_gb": 0,
            "gpus": 0,
        }
    ]

    # Act & Assert
    with pytest.raises(pydantic_core._pydantic_core.ValidationError):
        instance_response_to_dto(invalid_response)
