FROM python:3.9-slim

# Set environment variables
# Use PYTHONUNBUFFERED to ensure logs are not buffered and lost
# Use PYTHONDONTWRITEBYTECODE to prevent writing .pyc files
ENV PORT=8000 \
    PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1

# Set the working directory within the container
WORKDIR /app

# Copy only the requirements.txt file to leverage Docker cache
COPY requirements.txt .

# Install dependencies from requirements.txt file
# Using `--no-cache-dir` to avoid caching artifacts in the Docker layer, reducing image size
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the application's code
COPY src .

# Inform Docker that the container listens on the specified port at runtime.
EXPOSE $PORT

# Use exec form of CMD to help gunicorn receive SIGINT and SIGTERM for graceful shutdown
CMD ["sh", "-c", "gunicorn --bind 0.0.0.0:$PORT app:app --log-level debug --workers 4 --timeout 180"]
